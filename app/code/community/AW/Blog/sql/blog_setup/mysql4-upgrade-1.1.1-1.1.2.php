<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
$installer->run("
    ALTER TABLE `{$this->getTable('blog/comment')}` 
    ADD COLUMN `parent_id` INT(11) UNSIGNED NULL AFTER `comment_id`;
");
$installer->run("
    CREATE TABLE `{$this->getTable('blog/blog_product')}` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `post_id` INT(11) UNSIGNED NOT NULL,
    `product_id` INT(11) UNSIGNED NOT NULL,
    PRIMARY KEY (`id`))
    ENGINE = InnoDB;
");
$installer->endSetup();
