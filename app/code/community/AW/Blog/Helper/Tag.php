<?php
class AW_Blog_Helper_Tag extends Mage_Core_Helper_Abstract
{
    /**
     * Retrieve search url
     *
     * @return string
     */
    public function getSearchUrl()
    {
        return $this->_getUrl('blog/tag/search', array(
            '_secure' => Mage::app()->getStore()->isCurrentlySecure()
        ));
    }
    
    /**
     * Get q param from request
     * 
     * @return string
     */
    public function getQueryText()
    {
        return Mage::app()->getRequest()->getParam('q');
    }
}
