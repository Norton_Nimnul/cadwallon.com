<?php

class AW_Blog_Helper_Post extends Mage_Core_Helper_Abstract
{
    /**
     * Renders CMS page
     * Call from controller action
     *
     * @param Mage_Core_Controller_Front_Action $action
     * @param integer                              $identifier
     *
     * @return bool
     */
    public function renderPage(Mage_Core_Controller_Front_Action $action, $identifier = null)
    {
        $page = Mage::getModel('blog/post');
        if (!is_null($identifier) && $identifier !== $page->getId()) {
            $page->setStoreId(Mage::app()->getStore()->getId());
            if (!$page->load($identifier)) {
                return false;
            }
        }

        if (!$page->getId()) {
            return false;
        }
        if ($page->getStatus() == 2) {
            return false;
        }
        $pageTitle = Mage::getSingleton('blog/post')->load($identifier)->getTitle();
        $blogTitle = Mage::getStoreConfig('blog/blog/title') . " - ";

        $action->loadLayout();
        if ($storage = Mage::getSingleton('customer/session')) {
            $action->getLayout()->getMessagesBlock()->addMessages($storage->getMessages(true));
        }

        $action->getLayout()->getBlock('head')->setTitle($blogTitle . $pageTitle);
        $action->getLayout()->getBlock('root')->setTemplate(Mage::getStoreConfig('blog/blog/layout'));
        $action->renderLayout();

        return true;
    }
	
	  public function renderComments($block, $post, $comments, $enabledComments, $parentId = 0)
{
    $content = '';
    foreach ($comments as $comment) {
        if ($comment->getParentId() == $parentId) {
            $content .= '<div class="commentWrapper">';
            $content .= PHP_EOL;
            $content .= '<div class="commentDetails"><h4 class="username">' . $comment->getUser() . '</h4> ' . $this->__("posted on") . ' ' . $block->formatDate($comment->getCreatedTime(), Mage::getStoreConfig('blog/blog/dateformat'), true) . '</div>';
            $content .= PHP_EOL;
            $content .= '<div class="commentContent">' . nl2br($comment->getComment()) . '</div>';
            $content .= PHP_EOL;
            if ($enabledComments === true) {
                $content .= '<div class="commentReply"><a href="javascript:void(0);" data-entity-id="' . $comment->getId() . '"><span class="ccount">'.$this->__("Reply").'</span></a></div>';
                $content .= PHP_EOL;
                $content .= '<div class="commentForm"></div>';
                $content .= PHP_EOL;
            }
            $content .= '<div class="commentChilds" style="margin-left: 25px;">' . $this->renderComments($block, $post, $comments, $enabledComments, $comment->getId()) . '</div>';
            $content .= PHP_EOL;
            $content .= '</div>';
            $content .= PHP_EOL;
        }
    }
    return $content;
}
	
}