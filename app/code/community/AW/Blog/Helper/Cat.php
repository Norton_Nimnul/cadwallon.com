<?php

class AW_Blog_Helper_Cat extends Mage_Core_Helper_Abstract
{
    /**
     * Renders CMS page
     * Call from controller action
     *
     * @param Mage_Core_Controller_Front_Action $action
     * @param integer                           $identifier
     *
     * @return bool
     */
    public function renderPage(Mage_Core_Controller_Front_Action $action, $identifier = null)
    {
        if (!$catId = Mage::getSingleton('blog/cat')->load($identifier)->getCatId()) {
            return false;
        }

        $pageTitle = Mage::getSingleton('blog/cat')->load($identifier)->getTitle();
        $blogTitle = Mage::getStoreConfig('blog/blog/title') . " - " . $pageTitle;

        $action->loadLayout();
        if ($storage = Mage::getSingleton('customer/session')) {
            $action->getLayout()->getMessagesBlock()->addMessages($storage->getMessages(true));
        }
        $action->getLayout()->getBlock('head')->setTitle($blogTitle);

        $action->getLayout()->getBlock('root')->setTemplate(Mage::getStoreConfig('blog/blog/layout'));
        $action->renderLayout();

        return true;
    }
	
	 public function getCommentList($postId)
    { //echo $this->getPost()->getPost_coment(); die;
        if (!$this->hasData('commentCollection')) {
            $collection = Mage::getModel('blog/comment')
                ->getCollection()
                //->addPostFilter($this->getPost()->getPostId())
				//->addPostFilter($this->getPost()->getPost_coment())
				// ->addFilter('post_id', $this->getPost()->getPostId())
				 //->addFilter('post_id',$this->getPost()->getPost_coment())
				 ->addFieldToFilter('post_id', array($this->getPost()->getPostId(), $this->getPost()->getPost_coment()))
                ->setOrder('created_time', 'desc')
                ->addApproveFilter(2)
				
            ;
			//echo $collection->getSelect();
            $collection->setPageSize((int)Mage::helper('blog')->commentsPerPage());
            $this->setData('commentCollection', $collection);
        }
        return $this->getData('commentCollection');
    }
}