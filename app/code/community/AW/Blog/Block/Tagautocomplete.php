<?php
class AW_Blog_Block_Tagautocomplete extends AW_Blog_Block_Abstract
{
    /**
     * @var array
     */
    protected $_autoCompleteData;

    protected function _toHtml()
    {
        $html = '';

        if (!$this->_beforeToHtml()) {
            return $html;
        }
        
        if ($this->_autoCompleteData === null) {
            $this->_loadAutocompleteData();
        }
        
        $total = count($this->_data);
        
        if ($total === 0) {
            return $html;
        }
        
        $html .= '<ul><li style="display:none"></li>';
        foreach ($this->_autoCompleteData as $key => $item) {
            if ($key == 0) {
                $item['row_class'] .= ' first';
            }

            if ($key + 1 == $total) {
                $item['row_class'] .= ' last';
            }

            $html .=  '<li title="'.$this->escapeHtml($item['title']).'" class="'.$item['row_class'].'">'
                . '<span class="amount">'.$item['num_of_results'].'</span>'.$this->escapeHtml($item['title']).'</li>';
        }

        $html.= '</ul>';

        return $html;
    }

    protected function _loadAutocompleteData()
    {
        if (!$this->_suggestData) {
            $query = $this->helper('blog/tag')->getQueryText();
            $counter = 0;
            $data = array();
            foreach ($this->_getAutocompleteCollection() as $item) {
                $_data = array(
                    'title' => $item->getTag(),
                    'row_class' => (++$counter)%2?'odd':'even',
                    'num_of_results' => $item->getTagCount()
                );

                if ($item->getTag() == $query) {
                    array_unshift($data, $_data);
                }
                else {
                    $data[] = $_data;
                }
            }
            $this->_autoCompleteData = $data;
        }
    }
    
    protected function _getAutocompleteCollection()
    {
        $collection = Mage::getModel('blog/tag')->getCollection();
        $collection->getSelect()->where('tag LIKE ?', $this->helper('blog/tag')->getQueryText() . '%');
        $collection->getSelect()->where('tag_count > 0');
        $collection->getSelect()->where('store_id = ?', Mage::app()->getStore()->getStoreId());
        return $collection;
    }
}