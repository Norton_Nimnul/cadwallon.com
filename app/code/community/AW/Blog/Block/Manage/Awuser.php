<?php


class AW_Blog_Block_Manage_Awuser extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "manage_awuser";
	$this->_blockGroup = "blog";
	$this->_headerText = Mage::helper("blog")->__("AWUser Manager");
	$this->_addButtonLabel = Mage::helper("blog")->__("Add New Item");
	parent::__construct();
	
	}

}