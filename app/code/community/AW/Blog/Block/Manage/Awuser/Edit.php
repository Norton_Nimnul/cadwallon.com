<?php
	
class AW_Blog_Block_Manage_Awuser_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "blog";
				$this->_controller = "manage_awuser";
				$this->_updateButton("save", "label", Mage::helper("blog")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("blog")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("blog")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("awuser_data") && Mage::registry("awuser_data")->getId() ){

				    return Mage::helper("blog")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("awuser_data")->getId()));

				} 
				else{

				     return Mage::helper("blog")->__("Add Item");

				}
		}
}