<?php class AW_Blog_Block_Manage_Awuser_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row){
        $mediaurl=Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $value = $row->getData($this->getColumn()->getIndex());
        return '<img src="'.$mediaurl.DS.$value.'"  style="width:50px;height:50px;"/>';
    }
}