<?php
class AW_Blog_Block_Manage_Awuser_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				echo $fieldset = $form->addFieldset("blog_form", array("legend"=>Mage::helper("blog")->__("Item information")));

				
						$fieldset->addField("nameuser", "text", array(
						"label" => Mage::helper("blog")->__("Name user"),
						"name" => "nameuser",
						 'required' => true,
						));
							
							if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField(
                'store_id',
                'select',
                array(
                     'name'     => 'stores[]',
                     'label'    => Mage::helper('cms')->__('Store View'),
                     'title'    => Mage::helper('cms')->__('Store View'),
                     'required' => true,
                     'values'   => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
                )
            );
        }		
						$fieldset->addField('avatar', 'image', array(
						'label' => Mage::helper('blog')->__('Аvatar'),
						'name' => 'avatar',
						'note' => '(*.jpg, *.png, *.gif)',
						));
						$fieldset->addField("custom", "textarea", array(
						"label" => Mage::helper("blog")->__("Custom informations"),
						"name" => "custom",
						));
					

				if (Mage::getSingleton("adminhtml/session")->getAWUserData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getAWUserData());
					Mage::getSingleton("adminhtml/session")->setAWUserData(null);
				} 
				elseif(Mage::registry("awuser_data")) {
				    $form->setValues(Mage::registry("awuser_data")->getData());
				}
				return parent::_prepareForm();
		}
}
