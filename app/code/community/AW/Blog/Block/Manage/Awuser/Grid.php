<?php

class AW_Blog_Block_Manage_Awuser_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("awuserGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("blog/awuser")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("blog")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
				$this->addColumn("nameuser", array(
				"header" => Mage::helper("blog")->__("nameuser"),
				"index" => "nameuser",
				)); 
				
				 $this->addColumn('avatar', array(
            'header' => Mage::helper("blog")->__('Avatar'),
            
            'index' => 'avatar',
           
            'renderer' => 'AW_Blog_Block_Manage_Awuser_Renderer_Image'
        ));
				
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_awuser', array(
					 'label'=> Mage::helper('blog')->__('Remove AWUser'),
					 'url'  => $this->getUrl('*/adminhtml_awuser/massRemove'),
					 'confirm' => Mage::helper('blog')->__('Are you sure?')
				));
			return $this;
		}
			

}