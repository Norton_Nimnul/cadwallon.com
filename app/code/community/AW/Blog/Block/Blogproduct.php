<?php

class AW_Blog_Block_Blogproduct extends AW_Blog_Block_Abstract
{
    /**
     * @return AW_Blog_Model_Mysql4_Blog_Collection
     */
    protected function getProductsCollection() 
    {
        if (Mage::registry('post')) {
            return Mage::registry('post')->getProductsCollection();
        } else {
            return array();
        }
    }
	
	 protected function getProductsPostCollection($id) 
    {
		
		$collection=Mage::getModel('blog/post')->getCollection();
    $collection->getSelect()->join( array('table_alias'=>Mage::getSingleton('core/resource')->getTableName('blog/blogproduct')), 'main_table.post_id = table_alias.post_id')
   ->where('table_alias.product_id = ?', floatval($id));
	  foreach ($collection as $item) {
            $item->setAddress($this->getBlogUrl($item->getIdentifier()));
        }
	  return $collection; 
	   
    }
	
}