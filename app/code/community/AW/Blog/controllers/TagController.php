<?php
class AW_Blog_TagController extends Mage_Core_Controller_Front_Action
{
    public function preDispatch()
    {
        parent::preDispatch();

        if (!Mage::helper('blog')->getEnabled()) {
            $this->_redirectUrl(Mage::helper('core/url')->getHomeUrl());
        }
    }
    
    public function searchAction()
    {
        $q = $this->getRequest()->getParam('q', false);
        
        if (!empty($q) && is_string($q)) {
            $this->_redirectUrl(Mage::getUrl(Mage::helper('blog')->getRoute() . '/tag/' . urlencode($q)));
        } else {
            $this->_redirectUrl(Mage::getUrl(Mage::helper('blog')->getRoute() . '/tag/'));
        }
    }
    
    public function autocompleteAction()
    {
        if (!$this->getRequest()->getParam('q', false)) {
            $this->getResponse()->setRedirect(Mage::getSingleton('core/url')->getBaseUrl());
        }

        $this->getResponse()->setBody($this->getLayout()->createBlock('blog/tagautocomplete')->toHtml());
    }
}