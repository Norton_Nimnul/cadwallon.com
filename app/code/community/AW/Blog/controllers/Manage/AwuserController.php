<?php

class AW_Blog_Manage_AwuserController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("blog/awuser")->_addBreadcrumb(Mage::helper("adminhtml")->__("AWUser  Manager"),Mage::helper("adminhtml")->__("AWUser Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Blog"));
			    $this->_title($this->__("Manager AWUser"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Blog"));
				$this->_title($this->__("AWUser"));
			    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("blog/awuser")->load($id);
				if ($model->getId()) {
					Mage::register("awuser_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("blog/awuser");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("AWUser Manager"), Mage::helper("adminhtml")->__("AWUser Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("AWUser Description"), Mage::helper("adminhtml")->__("AWUser Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("blog/manage_awuser_edit"))->_addLeft($this->getLayout()->createBlock("blog/manage_awuser_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("blog")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Blog"));
		$this->_title($this->__("AWUser"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("blog/awuser")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("awuser_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("blog/awuser");
	//echo "sadasd";
		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("AWUser Manager"), Mage::helper("adminhtml")->__("AWUser Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("AWUser Description"), Mage::helper("adminhtml")->__("AWUser Description"));


		$this->_addContent($this->getLayout()->createBlock("blog/manage_awuser_edit"))->_addLeft($this->getLayout()->createBlock("blog/manage_awuser_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();

		
				if ($post_data) {

					try {

						
				 //save image
		try{

if((bool)$post_data['avatar']['delete']==1) {

	        $post_data['avatar']='';

}
else {

	unset($post_data['avatar']);

	if (isset($_FILES)){

		if ($_FILES['avatar']['name']) {

			if($this->getRequest()->getParam("id")){
				$model = Mage::getModel("blog/awuser")->load($this->getRequest()->getParam("id"));
				if($model->getData('avatar')){
						$io = new Varien_Io_File();
						$io->rm(Mage::getBaseDir('media').DS.implode(DS,explode('/',$model->getData('avatar'))));	
				}
			}
						$path = Mage::getBaseDir('media') . DS . 'blog' . DS .'awuser'.DS;
						$uploader = new Varien_File_Uploader('avatar');
						$uploader->setAllowedExtensions(array('jpg','png','gif'));
						$uploader->setAllowRenameFiles(false);
						$uploader->setFilesDispersion(false);
						$destFile = $path.$_FILES['avatar']['name'];
						$filename = $uploader->getNewFileName($destFile);
						$uploader->save($path, $filename);

						$post_data['avatar']='blog/awuser/'.$filename;
		}
    }
}

        } catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
        }
//save image
 
               $post_data['store_id']=$post_data['stores'][0];
            
			//print_r ($post_data['stores']); die;

						$model = Mage::getModel("blog/awuser")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("AWUser was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setAWUserData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setAWUserData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("blog/awuser");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("blog/awuser");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
			 
		/**
		 *  Export order grid to Excel XML format
		 */
		public function exportExcelAction()
		{
			$fileName   = 'awuser.xml';
			$grid       = $this->getLayout()->createBlock('blog/manage_awuser_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		}
}
