<?php

/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/LICENSE-L.txt
 *
 * @category   AW
 * @package    AW_Blog
 * @copyright  Copyright (c) 2009-2010 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/LICENSE-L.txt
 */
require_once 'recaptcha/recaptchalib-aw.php';

class AW_Blog_PostController extends Mage_Core_Controller_Front_Action {

    public function preDispatch() {

        parent::preDispatch();

        if (!Mage::helper('blog')->getEnabled()) {
            $this->_redirectUrl(Mage::helper('core/url')->getHomeUrl());
        }
    }

    protected function _validateData($data) {
        $errors = array();

        $helper = Mage::helper('blog');

        if (!Zend_Validate::is($data->getUser(), 'NotEmpty')) {
            $errors[] = $helper->__('Name can\'t be empty');
        }

        if (!Zend_Validate::is($data->getComment(), 'NotEmpty')) {
            $errors[] = $helper->__('Comment can\'t be empty');
        }

        if (!Zend_Validate::is($data->getPostId(), 'NotEmpty')) {
            $errors[] = $helper->__('post_id can\'t be empty');
        }

        $validator = new Zend_Validate_EmailAddress();
        if (!$validator->isValid($data->getEmail())) {
            $errors[] = $helper->__('"%s" is not a valid email address.', $data->getEmail());
        }

        return $errors;
    }


function get_client_ip() {
$ipaddress = '';
if (getenv('HTTP_CLIENT_IP'))
    $ipaddress = getenv('HTTP_CLIENT_IP');
elseif(getenv('HTTP_X_FORWARDED_FOR'))
    $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
elseif(getenv('HTTP_X_FORWARDED'))
    $ipaddress = getenv('HTTP_X_FORWARDED');
else if(getenv('HTTP_FORWARDED_FOR'))
    $ipaddress = getenv('HTTP_FORWARDED_FOR');
else if(getenv('HTTP_FORWARDED'))
    $ipaddress = getenv('HTTP_FORWARDED');
else if(getenv('REMOTE_ADDR'))
    $ipaddress = getenv('REMOTE_ADDR');
else
    $ipaddress = 'UNKNOWN';

return $ipaddress;  }


    public function viewAction() {

        $identifier = $this->getRequest()->getParam('identifier', $this->getRequest()->getParam('id', false));
        $helper = Mage::helper('blog');
        $session = Mage::getSingleton('customer/session'); 
	
	$collection3 = Mage::getModel('blog/post')->getCollection();
    $collection3->addFieldToFilter('identifier', $this->getRequest()->getParam('identifier'));
    foreach($collection3 as $data1)
    {
    $id_post= $data1->getid(); 
	 $kol_view = $data1->getviewed_post();
    }
	/*****************************/ 
	
	
	
	/******************************/
	


        if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('blog/comment');
             $data['user'] = strip_tags($data['user']);
            $model->setData($data);

            if (!Mage::getStoreConfig('blog/comments/enabled')) {
                $session->addError($helper->__('Comments are not enabled.'));
                if (!Mage::helper('blog/post')->renderPage($this, $identifier)) {
                    $this->_forward('NoRoute');
                }
                return;
            }


            if (!$session->isLoggedIn() && Mage::getStoreConfig('blog/comments/login')) {
                $session->addError($helper->__('You must be logged in to comment.'));
                if (!Mage::helper('blog/post')->renderPage($this, $identifier)) {
                    $this->_forward('NoRoute');
                }
                return;
            } else if ($session->isLoggedIn() && Mage::getStoreConfig('blog/comments/login')) {
                $model->setUser($helper->getUserName());
                $model->setEmail($helper->getUserEmail());
            }

            try {

                if (Mage::getStoreConfig('blog/recaptcha/enabled') && !$session->isLoggedIn()) {
                    $publickey = Mage::getStoreConfig('blog/recaptcha/publickey');
                    $privatekey = Mage::getStoreConfig('blog/recaptcha/privatekey');

                    $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $data["recaptcha_challenge_field"], $data["recaptcha_response_field"]);

                    if (!$resp->is_valid) {
                        if ($resp->error == "incorrect-captcha-sol") {
                            $session->addError($helper->__('Your Recaptcha solution was incorrect, please try again'));
                        } else {
                            $session->addError($helper->__('An error occured. Please try again'));
                        }
                        // Redirect back with error message
                        $session->setBlogPostModel($model);
                        $this->_redirectReferer();
                        return;
                    }
                }


                $errors = $this->_validateData($model);
                if (!empty($errors)) {
                    foreach ($errors as $error) {
                        $session->addError($error);
                    }
                    $this->_redirectReferer();
                    return;
                }

                if ($session->getData('blog_post_model')) {
                    $session->unsetData('blog_post_model');
                }
                $model->setCreatedTime(now());
                $model->setComment(htmlspecialchars($model->getComment(), ENT_QUOTES));
                if (Mage::getStoreConfig('blog/comments/approval')) {
                    $model->setStatus(2);
                    $session->addSuccess($helper->__('Your comment has been submitted.'));
                } else if ($session->isLoggedIn() && Mage::getStoreConfig('blog/comments/loginauto')) {
                    $model->setStatus(2);
                    $session->addSuccess($helper->__('Your comment has been submitted.'));
                } else {
                    $model->setStatus(1);
                    $session->addSuccess($helper->__('Your comment has been submitted and is awaiting approval.'));
                }
                $model->save();

                $comment_id = $model->getCommentId();
            } catch (Exception $e) {
                if (!Mage::helper('blog/post')->renderPage($this, $identifier)) {
                    $this->_forward('NoRoute');
                }
            }

            if (Mage::getStoreConfig('blog/comments/recipient_email') != null && $model->getStatus() == 1 && isset($comment_id)) {
                $translate = Mage::getSingleton('core/translate');
                /* @var $translate Mage_Core_Model_Translate */
                $translate->setTranslateInline(false);
                try {
                    $data["url"] = Mage::getUrl('blog/manage_comment/edit/id/' . $comment_id);
                    $postObject = new Varien_Object();
                    $postObject->setData($data);
                    $mailTemplate = Mage::getModel('core/email_template');
                    /* @var $mailTemplate Mage_Core_Model_Email_Template */
                    $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                            ->sendTransactional(
                                    Mage::getStoreConfig('blog/comments/email_template'), Mage::getStoreConfig('blog/comments/sender_email_identity'), Mage::getStoreConfig('blog/comments/recipient_email'), null, array('data' => $postObject)
                    );
                    $translate->setTranslateInline(true);
                } catch (Exception $e) {
                    $translate->setTranslateInline(true);
                }
            }
            $this->_redirectReferer();
            return;
            if (!Mage::helper('blog/post')->renderPage($this, $identifier)) {
                $this->_forward('NoRoute');
            }
        } else {
            /* GET request */
            if (!Mage::helper('blog/post')->renderPage($this, $identifier)) {
                $session->addNotice($helper->__('The requested page could not be found'));
                $this->_redirect($helper->getRoute());
                return false;
            }
        }
		
	/****************************INFO ********************************************************************/	
			
			
		//	echo $identifier;
		//	echo $this->get_client_ip();  
		//	echo $kol_view;
/*******************************INFO *****************************************************************/			
	

/*********************LOAD IP*************************/
$load_visotor = Mage::getModel('blog/visitor')->getCollection();
$load_visotor->addFieldToFilter('identifier', $this->getRequest()->getParam('identifier'))->
addFieldToFilter('ip_adreses', $this->get_client_ip())->load();
//echo count($load_visotor);
/*********************LOAD IP*************************/

		if (!count($load_visotor)) 
		{
		
		/*********************SAVE IP*************************/

$viewed_ad = Mage::getModel('blog/visitor');
$viewed_ad->setData('identifier',$this->getRequest()->getParam('identifier'));
$viewed_ad->setData('ip_adreses',$this->get_client_ip());
$viewed_ad->save();
//print_r ($viewed_ad);
/*********************SAVE IP*************************/
			$table_prefix = Mage::getConfig()->getTablePrefix();
    $tableName = $table_prefix.'aw_blog_visitor_post';
	 $resource = Mage::getSingleton('core/resource');
    $writeconnection = $resource->getConnection('core_write');
	

		
		$viewed_post = Mage::getModel('blog/visitorpost')->load($id_post);
				if ($viewed_post->getpost_id()==$id_post) {
				$viewed_post_= $viewed_post->getviewed_post();
				$tt = $viewed_post_+1;
				$sql = "update {$tableName} SET viewed_post = '{$tt}' WHERE post_id= '{$id_post}'";
			$writeconnection->query($sql);
				} else {
			$sql = "insert into {$tableName} (post_id, viewed_post) values ('$id_post', '1')";
			$writeconnection->query($sql);
				}
		
		}

/********************************************************************************************************/		
		
    }

    public function noRouteAction($coreRoute = null) {
        $this->getResponse()->setHeader('HTTP/1.1', '404 Not Found');
        $this->getResponse()->setHeader('Status', '404 File not found');

        $pageId = Mage::getStoreConfig('web/default/cms_no_route');
        if (!Mage::helper('cms/page')->renderPage($this, $pageId)) {
            $this->_forward('defaultNoRoute');
        }
    }

}
