<?php
/**
 * MageWorx
 * MageWorx SeoMarkup Extension
 *
 * @category   MageWorx
 * @package    MageWorx_SeoMarkup
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 */


class MageWorx_SeoMarkup_Helper_Config extends Mage_Core_Helper_Abstract
{
    const XML_PATH_PRODUCT_OG_ENABLED               = 'mageworx_seo/richsnippets/product_og_enabled';

    const XML_PATH_RICHSNIPPET_ENABLED              = 'mageworx_seo/richsnippets/enable';
    const XML_PATH_RICHSNIPPET_SELLER_ENABLED       = 'mageworx_seo/richsnippets/enable_seller';
    const XML_PATH_RICHSNIPPET_SKU_ENABLED          = 'mageworx_seo/richsnippets/enable_sku';
    const XML_PATH_RICHSNIPPET_SKU_CODE             = 'mageworx_seo/richsnippets/attribute_code_sku';
    const XML_PATH_RICHSNIPPET_PAYMENT_ENABLED      = 'mageworx_seo/richsnippets/enable_payment';
    const XML_PATH_RICHSNIPPET_DELIVERY_ENABLED     = 'mageworx_seo/richsnippets/enable_delivery';
    const XML_PATH_RICHSNIPPET_COLOR_ENABLED        = 'mageworx_seo/richsnippets/enable_color';
    const XML_PATH_RICHSNIPPET_COLOR_CODE           = 'mageworx_seo/richsnippets/attribute_code_color';
    const XML_PATH_RICHSNIPPET_HEIGHT_ENABLED       = 'mageworx_seo/richsnippets/enable_height';
    const XML_PATH_RICHSNIPPET_HEIGHT_CODE          = 'mageworx_seo/richsnippets/attribute_code_height';
    const XML_PATH_RICHSNIPPET_WIDTH_ENABLED        = 'mageworx_seo/richsnippets/enable_width';
    const XML_PATH_RICHSNIPPET_WIDTH_CODE           = 'mageworx_seo/richsnippets/attribute_code_width';
    const XML_PATH_RICHSNIPPET_DEPTH_ENABLED        = 'mageworx_seo/richsnippets/enable_depth';
    const XML_PATH_RICHSNIPPET_DEPTH_CODE           = 'mageworx_seo/richsnippets/attribute_code_depth';
    const XML_PATH_RICHSNIPPET_WEIGHT_ENABLED       = 'mageworx_seo/richsnippets/enable_weight';
    const XML_PATH_RICHSNIPPET_WEIGHT_UNIT          = 'mageworx_seo/richsnippets/weight_unit';
    const XML_PATH_RICHSNIPPET_MANUFACTURER_ENABLED = 'mageworx_seo/richsnippets/enable_manufacturer';
    const XML_PATH_RICHSNIPPET_MANUFACTURER_CODE    = 'mageworx_seo/richsnippets/attribute_code_manufacturer';
    const XML_PATH_RICHSNIPPET_BRAND_ENABLED        = 'mageworx_seo/richsnippets/enable_brand';
    const XML_PATH_RICHSNIPPET_BRAND_CODE           = 'mageworx_seo/richsnippets/attribute_code_brand';
    const XML_PATH_RICHSNIPPET_MODEL_ENABLED        = 'mageworx_seo/richsnippets/enable_model';
    const XML_PATH_RICHSNIPPET_MODEL_CODE           = 'mageworx_seo/richsnippets/attribute_code_model';
    const XML_PATH_RICHSNIPPET_GTIN_ENABLED         = 'mageworx_seo/richsnippets/enable_gtin';
    const XML_PATH_RICHSNIPPET_GTIN_CODE            = 'mageworx_seo/richsnippets/attribute_code_gtin';
    const XML_PATH_RICHSNIPPET_DIMENSIONS_ENABLED   = 'mageworx_seo/richsnippets/enable_dimensions';
    const XML_PATH_RICHSNIPPET_DIMENSIONS_UNIT      = 'mageworx_seo/richsnippets/dimensions_unit';
    const XML_PATH_RICHSNIPPET_CONDITION_ENABLED    = 'mageworx_seo/richsnippets/enable_condition';
    const XML_PATH_RICHSNIPPET_CONDITION_CODE       = 'mageworx_seo/richsnippets/attribute_code_condition';
    const XML_PATH_RICHSNIPPET_CONDITION_NEW        = 'mageworx_seo/richsnippets/condition_value_new';
    const XML_PATH_RICHSNIPPET_CONDITION_REF        = 'mageworx_seo/richsnippets/condition_value_refurbished';
    const XML_PATH_RICHSNIPPET_CONDITION_USED       = 'mageworx_seo/richsnippets/condition_value_used';
    const XML_PATH_RICHSNIPPET_CONDITION_DAMAGED    = 'mageworx_seo/richsnippets/condition_value_damaged';
    const XML_PATH_RICHSNIPPET_CONDITION_DEFAULT    = 'mageworx_seo/richsnippets/condition_value_default';
    const XML_PATH_RICHSNIPPET_CATEGORY_ENABLED     = 'mageworx_seo/richsnippets/enable_category';
    const XML_PATH_RICHSNIPPET_CATEGORY_DEEPEST     = 'mageworx_seo/richsnippets/category_deepest';

    function isOpenGraphProtocolEnabled()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_PRODUCT_OG_ENABLED);
    }


    function isRichsnippetEnabled()
    {
        if ((int) Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_ENABLED) === 1) {
            return true;
        }
        return false;
    }

    function isRichSnippetOnlyBreadcrumbsEnabled()
    {
        if ($this->isRichsnippetEnabled()) {
            return true;
        }
        else {
            if ((int) Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_ENABLED) === 2) {
                return true;
            }
        }
        return false;
    }

    function isRichsnippetDisabled()
    {
        if (Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_ENABLED) == false) {
            return true;
        }
        return false;
    }

    function isRichsnippetCategoryEnabled()
    {
        return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_CATEGORY_ENABLED);
    }

    function isRichsnippetCategoryDeepest()
    {
        return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_CATEGORY_DEEPEST);
    }

    function isRichsnippetSellerEnabled()
    {
        return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_SELLER_ENABLED);
    }

    function isRichsnippetConditionEnabled()
    {
        return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_CONDITION_ENABLED);
    }

    function getSkuAttributeCode()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_SKU_CODE));
    }

    function getConditionAttributeCode()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_CONDITION_CODE));
    }

    function getConditionValueForNew()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_CONDITION_NEW));
    }

    function getConditionValueForRefurbished()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_CONDITION_REF));
    }

    function getConditionValueForDamaged()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_CONDITION_DAMAGED));
    }

    function getConditionValueForUsed()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_CONDITION_USED));
    }

    function getConditionDefaultValue()
    {
        return Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_CONDITION_DEFAULT);
    }

    function isRichsnippetSkuEnabled()
    {
        return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_SKU_ENABLED);
    }

    function isRichsnippetPaymentEnabled()
    {
        return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_PAYMENT_ENABLED);
    }

    function isRichsnippetDeliveryEnabled()
    {
        return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_DELIVERY_ENABLED);
    }

    function isRichsnippetColorEnabled()
    {
        return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_COLOR_ENABLED);
    }

    function getColorAttributeCode()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_COLOR_CODE));
    }

    function isRichsnippetManufacturerEnabled()
    {
        return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_MANUFACTURER_ENABLED);
    }

    function getManufacturerAttributeCode()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_MANUFACTURER_CODE));
    }

    function isRichsnippetBrandEnabled()
    {
        return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_BRAND_ENABLED);
    }

    function getBrandAttributeCode()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_BRAND_CODE));
    }

    function isRichsnippetModelEnabled()
    {
        return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_MODEL_ENABLED);
    }

    function getModelAttributeCode()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_MODEL_CODE));
    }

    function isRichsnippetGtinEnabled()
    {
        return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_GTIN_ENABLED);
    }

    function getGtinAttributeCode()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_GTIN_CODE));
    }

    function isRichsnippetDimensionsEnabled()
    {
        return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_DIMENSIONS_ENABLED);
    }

    function getRichsnippetDimensionsUnit()
    {
        $unit = trim(Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_DIMENSIONS_UNIT));
        if (preg_match('/^[a-zA-Z]+$/', $unit)) {
            return $unit;
        }
        return false;
    }

    function isRichsnippetHeightEnabled()
    {
        if ($this->isRichsnippetDimensionsEnabled()) {
            return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_HEIGHT_ENABLED);
        }
        return false;
    }

    function getHeightAttributeCode()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_HEIGHT_CODE));
    }

    function isRichsnippetWidthEnabled()
    {
        if ($this->isRichsnippetDimensionsEnabled()) {
            return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_WIDTH_ENABLED);
        }
        return false;
    }

    function getWidthAttributeCode()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_WIDTH_CODE));
    }

    function isRichsnippetDepthEnabled()
    {
        if ($this->isRichsnippetDimensionsEnabled()) {
            return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_DEPTH_ENABLED);
        }
        return false;
    }

    function getDepthAttributeCode()
    {
        return trim(Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_DEPTH_CODE));
    }

    function isRichsnippetWeightEnabled()
    {
        return (bool) Mage::getStoreConfigFlag(self::XML_PATH_RICHSNIPPET_WEIGHT_ENABLED);
    }

    function getRichsnippetWeightUnit()
    {
        return (string) Mage::getStoreConfig(self::XML_PATH_RICHSNIPPET_WEIGHT_UNIT);
    }

    function isProductPage()
    {
        if (strpos(Mage::app()->getRequest()->getRequestUri(), 'catalog/product/view') === false) {
            return false;
        }
        return true;
    }
}
