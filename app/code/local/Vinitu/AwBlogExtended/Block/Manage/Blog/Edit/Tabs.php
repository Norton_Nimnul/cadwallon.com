<?php
/**
 * Vinitu.com
 *
 * @category    Vinitu
 * @package     Vinitu_AwBlogExtended
 * @version     $Id: Tabs.php 70 2015-05-20 13:02:06Z soroka $
 * @author      Dmytro Soroka <mail@dmytrosoroka.com>
 * @copyright   Copyright (c) 2015, Vinitu.com (http://www.vinitu.com). All Rights Reserved.
 * @license     http://www.vinitu.com/magento/license.html
 */

/**
 * Vinitu_AwBlogExtended_Block_Manage_Blog_Edit_Tabs
 *
 * @category    Vinitu
 * @package     Vinitu_AwBlogExtended
 * @version     $Id: Tabs.php 70 2015-05-20 13:02:06Z soroka $
 * @author      Dmytro Soroka <mail@dmytrosoroka.com>
 * @copyright   Copyright (c) 2015, Vinitu.com (http://www.vinitu.com). All Rights Reserved.
 * @license     http://www.vinitu.com/magento/license.html
 */
class Vinitu_AwBlogExtended_Block_Manage_Blog_Edit_Tabs extends AW_Blog_Block_Manage_Blog_Edit_Tabs
{
    protected function _beforeToHtml()
    {
        $tabs = new ArrayObject();

        $tabs->append(array(
            'form_section',
            array(
                'label' => Mage::helper('blog')->__('Post Information'),
                'title' => Mage::helper('blog')->__('Post Information'),
                'content' => $this->getLayout()->createBlock('blog/manage_blog_edit_tab_form')->toHtml(),
            )
        ));

        $tabs->append(array(
            'options_section',
            array(
                'label' => Mage::helper('blog')->__('Advanced Options'),
                'title' => Mage::helper('blog')->__('Advanced Options'),
                'content' => $this->getLayout()->createBlock('blog/manage_blog_edit_tab_options')->toHtml(),
            )
        ));

        Mage::dispatchEvent('Vinitu_AwBlogExtended_Block_Manage_Blog_Edit_Tabs',
            array('tabs' => $tabs)
        );

        foreach ($tabs as $tab) {
            $this->addTab($tab[0], $tab[1]);
        }

        return parent::_beforeToHtml();
    }
}