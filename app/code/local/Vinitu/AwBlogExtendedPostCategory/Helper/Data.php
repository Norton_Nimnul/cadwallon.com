<?php
/**
 * Vinitu.com
 *
 * @category    Vinitu
 * @package     Vinitu_AwBlogExtendedPostCategory
 * @version     $Id: Data.php 70 2015-05-20 13:02:06Z soroka $
 * @author      Dmytro Soroka <mail@dmytrosoroka.com>
 * @copyright   Copyright (c) 2015, Vinitu.com (http://www.vinitu.com). All Rights Reserved.
 * @license     http://www.vinitu.com/magento/license.html
 */

/**
 * Vinitu_AwBlogExtendedPostCategory_Helper_Data
 *
 * @category    Vinitu
 * @package     Vinitu_AwBlogExtendedPostCategory
 * @version     $Id: Data.php 70 2015-05-20 13:02:06Z soroka $
 * @author      Dmytro Soroka <mail@dmytrosoroka.com>
 * @copyright   Copyright (c) 2015, Vinitu.com (http://www.vinitu.com). All Rights Reserved.
 * @license     http://www.vinitu.com/magento/license.html
 */
class Vinitu_AwBlogExtendedPostCategory_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param int $categoryId
     * @param int $limit
     * @param null $order
     * @return AW_Blog_Model_Mysql4_Post_Collection
     */
    public function getPostByCategoryId($categoryId, $isanchor, $order = null)
	
    {
		 
		if ($isanchor) {$limit =	Mage::getStoreConfig('blog/blog/postlimitincatlayered');}
		else { $limit =  Mage::getStoreConfig('blog/blog/postlimitincat');}
	
		
        /* @var $postCollection AW_Blog_Model_Mysql4_Post_Collection */
        $postCollection = Mage::getModel('blog/post')->getCollection();

        $postCollection->getSelect()->limit($limit);

        if (!empty($order)) {
            switch (strtolower($order)) {
                case 'RAND':
                    $postCollection->getSelect()->order(new Zend_Db_Expr('RAND()'));
                    break;
                case 'ASC':
                    $postCollection->getSelect()->order('ASC');
                    break;
                case 'DESC':
                    $postCollection->getSelect()->order('DESC');
                    break;
                default:
                    break;
            }
        }

        $postCollection->getSelect()->joinLeft(
            array('cat_table' => Mage::getSingleton('core/resource')->getTableName('awblogextendedpostcategory/category')),
            'main_table.post_id = cat_table.post_id',
            array(
                'category_id'
            )
        );

        $postCollection->getSelect()->where('cat_table.category_id = ?', $categoryId);
		

        return $postCollection;
    }
}
