<?php
/**
 * Vinitu.com
 *
 * @category    Vinitu
 * @package     Vinitu_AwBlogExtendedPostCategory
 * @version     $Id: Observer.php 72 2015-05-20 13:46:56Z soroka $
 * @author      Dmytro Soroka <mail@dmytrosoroka.com>
 * @copyright   Copyright (c) 2015, Vinitu.com (http://www.vinitu.com). All Rights Reserved.
 * @license     http://www.vinitu.com/magento/license.html
 */

/**
 * Vinitu_AwBlogExtendedPostCategory_Model_Observer
 *
 * @category    Vinitu
 * @package     Vinitu_AwBlogExtendedPostCategory
 * @version     $Id: Observer.php 72 2015-05-20 13:46:56Z soroka $
 * @author      Dmytro Soroka <mail@dmytrosoroka.com>
 * @copyright   Copyright (c) 2015, Vinitu.com (http://www.vinitu.com). All Rights Reserved.
 * @license     http://www.vinitu.com/magento/license.html
 */
class Vinitu_AwBlogExtendedPostCategory_Model_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     * @return Vinitu_AwBlogExtendedPostCategory_Model_Observer
     */
    public function cacheEditForm(Varien_Event_Observer $observer)
    {
        if (!($observer->getData('block') instanceof AW_Blog_Block_Manage_Blog_Edit_Tabs)) {
            return $this;
        }

        return $this;
    }

    /**
     * @param Varien_Event_Observer $observer
     * @return Vinitu_AwBlogExtendedPostCategory_Model_Observer
     */
    public function postAfterSave(Varien_Event_Observer $observer)
    {
        if (!($observer->getData('object') instanceof AW_Blog_Model_Post)) {
            return $this;
        }

        /* @var $model AW_Blog_Model_Post */
        $model = $observer->getData('object');

        $tableName = Mage::getSingleton('core/resource')->getTableName('awblogextendedpostcategory/category');

        $request = Mage::app()->getRequest();
        if ($request->has('category_ids')) {
            $ids = array();
            foreach (explode(',', strval($request->getParam('category_ids'))) as $id) {
                if(is_numeric($id)) {
                    array_push($ids, intval($id));
                }
            }

            $ids = array_unique($ids);

            /* @var $writeAdapter Varien_Db_Adapter_Interface */
            $writeAdapter = Mage::getSingleton('core/resource')->getConnection('core_write');
            $writeAdapter->delete(
                $tableName,
                $writeAdapter->quoteInto('post_id = ?', $model->getId())
            );

            foreach ($ids as $id) {
                $writeAdapter->insert(
                    $tableName,
                    array(
                        'post_id' => $model->getId(),
                        'category_id' => $id,
                        'position' => 1
                    )
                );
            }
        }

        return $this;
    }

    /**
     * @param Varien_Event_Observer $observer
     * @return Vinitu_AwBlogExtendedPostCategory_Model_Observer
     */
    public function postAfterLoad(Varien_Event_Observer $observer)
    {
        if (!($observer->getData('object') instanceof AW_Blog_Model_Blog)) {
            return $this;
        }

        /* @var $model AW_Blog_Model_Blog */
        $model = $observer->getData('object');

        if (!$model->getId()) {
            return $this;
        }

        $tableName = Mage::getSingleton('core/resource')->getTableName('awblogextendedpostcategory/category');

        /* @var $readAdapter Varien_Db_Adapter_Interface */
        $readAdapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $select = $readAdapter->select()->from($tableName, 'category_id')->where('post_id = ?', $model->getId());

        $ids = array();
        foreach ($select->query()->fetchAll(Zend_Db::FETCH_ASSOC) as $row) {
            $ids[] = $row['category_id'];
        }

        $model->setData('category_ids', $ids);

        return $this;
    }

    /**
     * @param Varien_Event_Observer $observer
     * @return Vinitu_AwBlogExtendedPostCategory_Model_Observer
     */
    public function addTab(Varien_Event_Observer $observer)
    {
        if (!($observer->getData('tabs') instanceof ArrayObject)) {
            return $this;
        }

        $tabAlreadyRegistered = false;

        foreach ($observer->getData('tabs') as $tab) {
            if ($tab[0] === 'vinitu_awblogextendedpostcategory_section') {
                $tabAlreadyRegistered = true;
                break;
            }
        }

        if (!$tabAlreadyRegistered) {
            $observer->getData('tabs')->append(array(
                'vinitu_awblogextendedpostcategory_section',
                array(
                    'label' => Mage::helper('awblogextendedpostcategory')->__('Associated categories'),
                    'url' => Mage::helper('adminhtml')->getUrl('awblogextendedpostcategory/manage_post/categories', array('_current' => true)),
                    'class' => 'ajax'
                )
            ));
        }

        return $this;
    }
}
