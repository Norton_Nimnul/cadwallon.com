<?php
/**
 * Vinitu.com
 *
 * @category    Vinitu
 * @package     Vinitu_AwBlogExtendedPostCategory
 * @version     $Id: mysql4-install-1.0.0.php 70 2015-05-20 13:02:06Z soroka $
 * @author      Dmytro Soroka <mail@dmytrosoroka.com>
 * @copyright   Copyright (c) 2015, Vinitu.com (http://www.vinitu.com). All Rights Reserved.
 * @license     http://www.vinitu.com/magento/license.html
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()->newTable($this->getTable('awblogextendedpostcategory/category'));

$table->addColumn('rel_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Relation ID');

$table->addColumn('post_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Post ID');

$table->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Category ID');

$table->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        'default'   => '0',
    ), 'Position');
$table->addIndex($this->getIdxName('awblogextendedpostcategory/category', array('category_id')), array('category_id'));

$table->addForeignKey(
    $this->getFkName('awblogextendedpostcategory/category', 'post_id', 'blog/post', 'entity_id'),
    'post_id',
    $this->getTable('blog/post'),
    'post_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE,
    Varien_Db_Ddl_Table::ACTION_CASCADE
);

$table->addForeignKey(
    $this->getFkName('awblogextendedpostcategory/category', 'category_id', 'catalog/category', 'entity_id'),
    'category_id',
    $this->getTable('catalog/category'),
    'entity_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE,
    Varien_Db_Ddl_Table::ACTION_CASCADE
);

$table->addIndex(
        $this->getIdxName(
            'awblogextendedpostcategory/category',
            array('post_id', 'category_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('post_id', 'category_id'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
);

$table->setComment('Post to Category Linkage Table');

$installer->getConnection()->createTable($table);

$installer->endSetup();
