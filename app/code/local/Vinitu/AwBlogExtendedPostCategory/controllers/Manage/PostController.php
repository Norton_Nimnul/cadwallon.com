<?php
/**
 * Vinitu.com
 *
 * @category    Vinitu
 * @package     Vinitu_AwBlogExtendedPostCategory
 * @version     $Id: PostController.php 70 2015-05-20 13:02:06Z soroka $
 * @author      Dmytro Soroka <mail@dmytrosoroka.com>
 * @copyright   Copyright (c) 2015, Vinitu.com (http://www.vinitu.com). All Rights Reserved.
 * @license     http://www.vinitu.com/magento/license.html
 */

/**
 * Vinitu_AwBlogExtendedPostCategory_Manage_PostController
 *
 * @category    Vinitu
 * @package     Vinitu_AwBlogExtendedPostCategory
 * @version     $Id: PostController.php 70 2015-05-20 13:02:06Z soroka $
 * @author      Dmytro Soroka <mail@dmytrosoroka.com>
 * @copyright   Copyright (c) 2015, Vinitu.com (http://www.vinitu.com). All Rights Reserved.
 * @license     http://www.vinitu.com/magento/license.html
 */
class Vinitu_AwBlogExtendedPostCategory_Manage_PostController extends Mage_Adminhtml_Controller_Action
{
    protected function _initEntity()
    {
        $id = intval($this->getRequest()->getParam('id'));
        $model = Mage::getModel('blog/blog');

        if ($id) {
            $model->load($id);
        }
        Mage::register('current_post', $model);
        return $model;
    }

    public function categoriesAction()
    {
        $this->_initEntity();
        $this->loadLayout();
        $this->renderLayout();
    }

    public function categoriesJsonAction()
    {
        $this->_initEntity();
        $json = $this->getLayout()->createBlock('awblogextendedpostcategory/manage_blog_edit_tab_categories')->getCategoryChildrenJson($this->getRequest()->getParam('category'));
        $this->getResponse()->setBody($json);
    }
}
