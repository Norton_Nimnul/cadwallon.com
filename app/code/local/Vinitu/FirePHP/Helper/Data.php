<?php
/**
 * @category    Vinitu
 * @package     Vinitu_FirePHP
 * @version     $Id: Data.php 63 2015-05-18 12:54:19Z soroka $
 * @author      Dmytro Soroka
 * @copyright   Copyright (c) 2015, Vinitu.com (http://www.vinitu.com). All Rights Reserved.
 */

/**
 * Vinitu_FirePHP_Helper_Data
 *
 * @category    Vinitu
 * @package     Vinitu_FirePHP
 * @version     $Id: Data.php 63 2015-05-18 12:54:19Z soroka $
 * @author      Dmytro Soroka
 * @copyright   Copyright (c) 2015, Vinitu.com (http://www.vinitu.com). All Rights Reserved.
 */
class Vinitu_FirePHP_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function __construct()
    {
        if (!class_exists('FirePHP', false)) {
            require_once(Mage::getBaseDir('lib') . DS . 'FirePHPCore' . DS . 'FirePHP.class.php');


            Zend_Wildfire_Channel_HttpHeaders::getInstance()->setRequest(Mage::app()->getRequest())->setResponse(Mage::app()->getResponse());
        }
    }

    /**
     * @param mixed $data
     * @return Vinitu_FirePHP_Helper_Data
     */
    public function log($data)
    {
        $firePHP = Zend_Wildfire_Plugin_FirePhp::getInstance();

        $firePHP->send($data);

        Zend_Wildfire_Channel_HttpHeaders::getInstance()->flush();
        Mage::app()->getResponse()->sendHeaders();

        return $this;
    }
}
